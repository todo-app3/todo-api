FROM node:12.16.3-alpine3.11

WORKDIR /home/node/app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .

ENTRYPOINT [ "sh", "-c", "\"start.sh\"" ]
