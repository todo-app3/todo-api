import test from 'ava';
import sinon from 'sinon';
import jwt from 'jsonwebtoken';
import verifyJwt from 'Middleware/VerifyJwt';

const sandbox = sinon.createSandbox();

test.afterEach(() => {
  sinon.restore();
  sandbox.restore();
});

test.serial('VerifyJwt (Happy)', t => {
  const mockRes = {
    locals: {}
  };
  const mockClaims = {
    uid: 'fake-uid',
    exp: 10
  };
  const mockCurrentTime = 5;
  sandbox.stub(jwt, 'verify').returns(mockClaims as unknown as void);
  sandbox.stub(Date, 'now').returns(mockCurrentTime);
  const stubNext = sandbox.stub();

  verifyJwt({
    cookies: {}
  } as any, mockRes as any, stubNext);

  t.assert(stubNext.calledOnce);
});

test.serial('VerifyJwt (Token Expired)', t => {
  const mockSend = {
    send: sinon.stub()
  };
  const stubStatus = sandbox.stub().returns(mockSend);
  const mockRes = {
    status: stubStatus
  };
  const mockClaims = {
    uid: 'fake-uid',
    exp: 4
  };
  const mockCurrentTime = 5;
  sandbox.stub(jwt, 'verify').returns(mockClaims as unknown as void);
  sandbox.stub(Date, 'now').returns(mockCurrentTime);
  const stubNext = sandbox.stub();

  verifyJwt({
    cookies: {}
  } as any, mockRes as any, stubNext);

  t.assert(stubNext.notCalled);
  t.assert(stubStatus.calledOnceWith(401));
});

test.serial('VerifyJwt (Invalid token)', t => {
  const mockSend = {
    send: sinon.stub()
  };
  const stubStatus = sandbox.stub().returns(mockSend);
  const mockRes = {
    status: stubStatus
  };
  sandbox.stub(jwt, 'verify').throws();
  const stubNext = sandbox.stub();

  verifyJwt({
    cookies: {}
  } as any, mockRes as any, stubNext);

  t.assert(stubNext.notCalled);
  t.assert(stubStatus.calledOnceWith(401));
});
