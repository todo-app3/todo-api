require('dotenv').config();

const { MYSQL_CONNECTION_STRING } = process.env;

module.exports = {
  client: 'mysql',
  connection: MYSQL_CONNECTION_STRING,
  migrations: {
    tableName: 'migrations',
    directory: './migrations'
  }
};
