exports.up = async function(knex) {
  await knex.schema.createTable('items', table => {
    table.boolean('isComplete')
    table.boolean('isArchived')
    table.string('title')
    table.text('description')
    // table.integer('userId').notNullable();
    // table.foreign('userId').references('id').inTable('user');
  });
};

exports.down = async function(knex) {
  await knex.schema.dropTable('items')
};