exports.up = async function(knex) {
  await knex.schema.alterTable('items', table => {
    table.string('uid').unique()
  });
};

exports.down = async function(knex) {
  await knex.schema.alterTable('items', table => {
    table.dropColumn('uid')
  });
};