exports.up = async function(knex) {
  await knex.schema.createTable('users', table => {
    table.increments('id')
    table.string('uid', 24)
    table.string('email')
    table.string('password')
  });
};

exports.down = async function(knex) {
  await knex.schema.dropTable('users')
};