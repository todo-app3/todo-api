exports.up = async function(knex) {
  await knex.schema.alterTable('items', table => {
    table.string('userUid')
  });
};

exports.down = async function(knex) {
  await knex.schema.alterTable('items', table => {
    table.dropColumn('userUid')
  });
};