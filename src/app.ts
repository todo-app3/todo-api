import cors from 'cors';
import express, { Response, Request, NextFunction } from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import HealthCheckController from 'Controllers/HealthCheckController';
import ItemRouter from './routers/ItemRouter';
import IdentityRouter from './routers/IdentityRouter';

const app = express();
const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
  methods: ['GET', 'OPTIONS', 'POST', 'PUT', 'HEAD', 'DELETE']
};
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors(corsOptions));

app.get('/', (req: Request, res: Response, unusedNext: NextFunction) => {
  return HealthCheckController.index(req, res);
});

const itemRouter = new ItemRouter;
app.use(itemRouter.getRoutePrefix(), itemRouter.getRouter());
const identityRouter = new IdentityRouter;
app.use(identityRouter.getRoutePrefix(), identityRouter.getRouter());

export default app;
