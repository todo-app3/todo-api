import express from 'express';

export default abstract class Router {
  protected router = express.Router();
  protected routePrefix: string;
  constructor(routePrefix: string) {
    this.routePrefix = routePrefix;
  }

  getRouter(): express.Router {
    return this.router;
  }

  getRoutePrefix(): string {
    return this.routePrefix;
  }
}
