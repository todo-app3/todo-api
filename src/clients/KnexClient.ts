import knexClient from 'knex';

export const createConnection = (): knexClient => {
  const {
    MYSQL_CONNECTION_STRING
  } = process.env;

  const knex = knexClient({
    client: 'mysql',
    connection: MYSQL_CONNECTION_STRING
  });

  return knex;
};

export default class KnexClient {
  private static dbConnection: knexClient;
  constructor() {
    if (!KnexClient.dbConnection) {
      KnexClient.dbConnection = createConnection();
    }
  }

  getConnection(): knexClient {
    return KnexClient.dbConnection;
  }
}
