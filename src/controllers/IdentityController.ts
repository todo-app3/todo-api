import { Request, Response } from 'express';
import IdentityService from 'Services/IdentityService';

export default class IdentityController {
  static async signin(req: Request, res: Response): Promise<Response> {
    const {
      email,
      password
    } = req.body;

    const {
      newUser,
      sessionToken
    } = await (new IdentityService).signin(email, password);
    const maxAge = 60 * 60 * 8 * 1000; // 8hrs

    return res.status(200).cookie('session', sessionToken, { maxAge }).json({
      newUser
    });
  }
}
