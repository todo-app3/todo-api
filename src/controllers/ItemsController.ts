import { Request, Response } from 'express';
import ItemService from 'Services/ItemService';

export default class ItemsController {
  static async createItem(req: Request, res: Response): Promise<Response> {
    const {
      title,
      description
    } = req.body;
    const { uid } = res.locals;

    const success = await (new ItemService(uid)).createItem(title, description);

    return res.status(success ? 200 : 500).json({
      success
    });
  }

  static async getItems(req: Request, res: Response): Promise<Response> {
    const {
      page
    } = req.query;
    const { uid } = res.locals;

    const itemService = new ItemService(uid);
    const items = await itemService.getItems(page);
    const itemCount = await itemService.countItems();

    return res.status(200).json({
      items,
      total: itemCount
    });
  }

  static async deleteItems(req: Request, res: Response): Promise<Response> {
    const {
      uids
    } = req.body;
    const { uid } = res.locals;

    await (new ItemService(uid)).deleteItems(uids);

    return res.status(200).json({
      success: true
    });
  }
}
