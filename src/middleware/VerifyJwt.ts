import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

const {
  JWT_SECRET
} = process.env;

export default (req: Request, res: Response, next: NextFunction): Response|void => {
  const {
    session
  } = req.cookies;
  try {
    type JwtClaims = {
      uid: string;
      exp: number;
    }
    const {
      uid,
      exp
    } = jwt.verify(session, JWT_SECRET as string) as JwtClaims;
    const currentTime = Date.now() / 1000;
    if (currentTime >= exp) {
      return res.status(401).send('Unauthenticated');
    }
    res.locals.uid = uid;
    return next();
  } catch(unusedError) {
    return res.status(401).send('Unauthenticated');
  }
};
