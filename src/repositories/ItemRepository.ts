import KnexClient from 'Clients/KnexClient';
import { QueryBuilder } from 'knex';
import crypto from 'crypto';

export interface TodoItem {
  title: string;
  description: string;
}

export default class ItemRepository {
  insertItem(userUid: string, title: string, description: string): Promise<QueryBuilder<any, number[]>> {
    const knex = (new KnexClient).getConnection();
    const uid = crypto.randomBytes(16).toString('base64');
    return knex('items').insert({
      title,
      description,
      uid,
      userUid
    });
  }

  async getItemRecords(userUid: string, page = 1): Promise<TodoItem[]> {
    const knex = (new KnexClient).getConnection();
    const records = await knex('items')
      .select('*')
      .offset((page-1) * 3)
      .where('userUid', userUid)
      .limit(3);

    return records;
  }

  async deleteItemRecords(userUid: string, uids: string[]): Promise<void> {
    const knex = (new KnexClient).getConnection();
    await knex('items')
      .delete()
      .whereIn('uid', uids)
      .andWhere('userUid', userUid);
  }

  async countItems(userUid: string): Promise<number> {
    const knex = (new KnexClient).getConnection();
    const [countResponse] = await knex('items')
      .where('userUid', userUid)
      .count();
    const count = countResponse['count(*)'];

    return count;
  }
}
