import KnexClient from 'Clients/KnexClient';
import Knex, { QueryBuilder } from 'knex';

interface User {
  email: string;
  /**
   * @property {string} password: A sha256 hash of the user's password
   */
  password: string;
  uid: string;
}

export interface UserRecord extends User {
  id: number;
}

export default class UserRepository {
  private knex: Knex;
  constructor() {
    this.knex = (new KnexClient).getConnection();
  }

  insertUser(data: User): Promise<QueryBuilder<any, number[]>> {
    return this.knex('users').insert(data);
  }

  getUserByEmail(email: string): Promise<UserRecord|undefined> {
    return this.knex('users')
      .select('*')
      .where('email', email)
      .first();
  }

  getUserByUid(uid: string): Promise<UserRecord|undefined> {
    return this.knex('users')
      .select('*')
      .where('uid', uid)
      .first();
  }
}
