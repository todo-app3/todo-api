import { Response, Request, NextFunction } from 'express';
import IdentityController from 'Controllers/IdentityController';
import Router from 'Classes/Router';

export default class IdentityRouter extends Router {
  constructor() {
    super('/identity');
    this.setupRoutes();
  }

  setupRoutes(): void {
    this.router.put('/', (req: Request, res: Response, unusedNext: NextFunction) => {
      return IdentityController.signin(req, res);
    });
  }
}
