import { Response, Request, NextFunction } from 'express';
import ItemsController from 'Controllers/ItemsController';
import Router from 'Classes/Router';
import VerifyJwt from 'Middleware/VerifyJwt';

export default class ItemRouter extends Router {
  constructor() {
    super('/items');
    this.setupRoutes();
  }

  setupRoutes(): void {
    this.router.use(VerifyJwt);
    this.router.post('/', (req: Request, res: Response, unusedNext: NextFunction) => {
      return ItemsController.createItem(req, res);
    });

    this.router.get('/', (req: Request, res: Response, unusedNext: NextFunction) => {
      return ItemsController.getItems(req, res);
    });

    this.router.delete('/', (req: Request, res: Response, unusedNext: NextFunction) => {
      return ItemsController.deleteItems(req, res);
    });
  }
}
