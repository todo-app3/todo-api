import UserRepository from 'Repositories/UserRepository';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';

interface SigninResult {
  /**
   * @property {boolean} newUser: Indiates whether a new user was created from this signin.
   */
  newUser: boolean;
  /**
   * @property {string} sessionToken: A JWT containing the userID for the now signed in user.
   */
  sessionToken: string;
}

const {
  JWT_SECRET
} = process.env;


export default class IdentityService {
  async signin(email: string, password: string): Promise<SigninResult> {
    const userRepo = new UserRepository;
    const existingUserRecord = await userRepo.getUserByEmail(email);
    const passwordHash = crypto.createHash('sha256').update(password).digest().toString('base64');
    const result: Partial<SigninResult> = {};
    let uid;

    if (existingUserRecord) {
      result.newUser = false;
      uid = existingUserRecord.uid;
    } else {
      uid = crypto.randomBytes(16).toString('base64');
      result.newUser = true;
      await userRepo.insertUser({
        email,
        password: passwordHash,
        uid
      });
    }

    result.sessionToken = jwt.sign({
      uid
    }, JWT_SECRET as string, { expiresIn: '8h' });

    return result as SigninResult;
  }
}
