import ItemRepository, { TodoItem } from 'Repositories/ItemRepository';
import { UserRecord } from 'Repositories/UserRepository';

export default class ItemService {
  private uid: string;
  private user: UserRecord|null = null;
  constructor(uid: string) {
    this.uid = uid;
  }

  async createItem(title: string, description: string): Promise<boolean> {
    let success = true;

    await (new ItemRepository).insertItem(this.uid, title, description).catch((error) => {
      console.error(error);
      success = false;
    });

    return success;
  }

  async getItems(page: number): Promise<TodoItem[]> {
    const itemRecords = await (new ItemRepository).getItemRecords(this.uid, page);

    return itemRecords;
  }

  async deleteItems(uids: string[]): Promise<void> {
    await (new ItemRepository).deleteItemRecords(this.uid, uids);
  }

  async countItems(): Promise<number> {
    const itemsCount = await (new ItemRepository).countItems(this.uid);
    return itemsCount;
  }
}
