#!/bin/sh

npx knex migrate:latest &&
npx nodemon --watch src -x node -r ts-node/register/transpile-only -r tsconfig-paths/register --inspect=0.0.0.0:5864 ./src/server.ts
